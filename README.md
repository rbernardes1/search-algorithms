# search-algorithms

## Description

This project contains search algorithms used on classic AI problems.

- **Vacuum Cleaner problem:**

A vacuum cleaner has two different places to clean. User defines if it is clean or dirty and where the vacuum cleaner begins his search.

## What is necessary to run this project
To run this project it is necessary to have `Python` and `Git`.

- The download of Git is available on this [link](https://git-scm.com/downloads).

- The download of Python is available on this [link](https://www.python.org/downloads/). 

- To access the project and clone through `cmd`

`git clone https://gitlab.com/rbernardes1/search-algorithms.git`


## Authors
- [**Renata Bernardes**](https://gitlab.com/rbernardes1)


