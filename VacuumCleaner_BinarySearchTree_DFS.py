import sys


class No (object):

# criação da classe nó que contem todos as informações necessarias

    def __init__(self, id, custo, pos_aspirador, estado_A, estado_B, level, seq_acoes, cont_filhos):
        self.id = id #id do nó
        self.custo = custo #custo do caminho até o nó
        self.pos_aspirador = pos_aspirador #posiçao do aspirador
        self.estado_A = estado_A #estado de A
        self.estado_B = estado_B #estado de B
        self.level = level #nível do nó
        self.seq_acoes = seq_acoes #sequencia de ações que levaram àquele nó
        self.cont_filhos = cont_filhos #quantos filhos esse nó ja gerou

def gera_filhos_busca_profundidade(n_pai, borda, lista_borda):
    ultimo_id = max(lista_borda) #pega o maior valor da lista de ids criados
    fila = borda

# se o nível do pai for menor que 4 pode-se expandir o mesmo
    if (n_pai.level < 4):
        if (n_pai.cont_filhos == 0): #se o pai não gerou nenhum filho ainda
            # vai gerar o filho da Esquerda
            id_filho_E = ultimo_id+1 #o valor do ID filho gerado é o maior id criado +1
            pos_aspirador_filho_E = 0 #independente se o pai está na esquerda ou na direita, o filho vai se deslocar para a esquerda
            estado_A_filho_E = n_pai.estado_A #como nao aspirou nenhum local, o estado de A e B continuam os mesmos
            estado_B_filho_E = n_pai.estado_B
            custo_filho_E = n_pai.custo + 1 # o custo do filho é o custo do pai + 1
            level_filho_E = n_pai.level+1 # o nível do filho é o nível do pai +1
            acao_filho_E = n_pai.seq_acoes +"Esquerda" # a sequencia de ações que gerou o filho é a mesma do pai, acrescentado do deslocamento para a esquerda
            n_pai.cont_filhos = n_pai.cont_filhos+1 # incrementa quantos filhos o pai gerou
            # insere o filho na primeira posição da borda
            filho_E = No(id_filho_E,custo_filho_E,pos_aspirador_filho_E,estado_A_filho_E,estado_B_filho_E,level_filho_E,acao_filho_E, 0)
            fila.insert(0,filho_E)
            # insere o id criado do filho na lista de ids
            lista_borda.append(id_filho_E)
            # insere o pai na segunda posição da borda pois ele vai ser tratado novamente
            pai_E = No(n_pai.id, n_pai.custo, n_pai.pos_aspirador, n_pai.estado_A, n_pai.estado_B, n_pai.level, n_pai.seq_acoes, n_pai.cont_filhos)
            fila.insert(1,pai_E)
            print ("filho E",n_pai.id,id_filho_E,pos_aspirador_filho_E, estado_A_filho_E, estado_B_filho_E, level_filho_E, acao_filho_E)
            return fila, lista_borda

        elif (n_pai.cont_filhos == 1): # se o pai gerou apenas um filho
            # vai gerar o filho Limpar
            id_filho_L = ultimo_id+1 # o valor do ID do filho gerado é o maior id criado +1
            pos_aspirador_filho_L = n_pai.pos_aspirador # o aspirador continua na mesma posição que o nó pai
            if (pos_aspirador_filho_L == 0): # se o aspirador está no espaço A
                estado_A_filho_L = 0 # limpa o estado A, independente do estado anterior de A
                estado_B_filho_L = n_pai.estado_B # o estado B permanece o mesmo do pai
            else: # se o aspirador está no espaço B
                estado_A_filho_L = n_pai.estado_A # o estado A permanece o mesmo do pai
                estado_B_filho_L = 0 # limpa o estado B, independente do estado anterior de B
            custo_filho_L = n_pai.custo+1 # o custo do filho é o custo do pai +1
            level_filho_L = n_pai.level+1 # o nível do filhho é o nível do pai +1
            acao_filho_L = n_pai.seq_acoes + "Limpar" # a sequencia de ações que gerou o filho é a mesma do pai, acrescentado da ação limpar
            n_pai.cont_filhos = n_pai.cont_filhos+1 # incrementa quantos filhos o pai gerou
            # insere o filho na primeira posição da borda
            filho_L = No(id_filho_L,custo_filho_L,pos_aspirador_filho_L,estado_A_filho_L,estado_B_filho_L,level_filho_L,acao_filho_L,0)
            fila.insert(0,filho_L)
            # insere o id criado do filho na lista de ids
            lista_borda.append(id_filho_L)
            # insere o pai na segunda posição da borda pois ele vai ser tratado novamente
            pai_L = No(n_pai.id, n_pai.custo, n_pai.pos_aspirador, n_pai.estado_A, n_pai.estado_B, n_pai.level, n_pai.seq_acoes, n_pai.cont_filhos)
            fila.insert(1,pai_L)
            print ("filho L",n_pai.id,id_filho_L,pos_aspirador_filho_L, estado_A_filho_L, estado_B_filho_L, level_filho_L, acao_filho_L)
            return fila, lista_borda

        elif (n_pai.cont_filhos == 2): # se o pai ja gerou 2 filhos
            # vai gerar o filho da Direita
            id_filho_D = ultimo_id+1 # o valor do ID do filho gerado é o maior id criado +1
            pos_aspirador_filho_D = 1 # independente se o pai está na esquerda ou direita, o filho vai estar na direita
            estado_A_filho_D = n_pai.estado_A #o estado de A continua o mesmo do pai
            estado_B_filho_D = n_pai.estado_B #o estado de B continua o mesmo do pai
            custo_filho_D = n_pai.custo + 1 # o custo do filho é o mesmo custo do pai + 1
            level_filho_D = n_pai.level+1 # o nível do filho é o mesmo nível do pai + 1
            acao_filho_D = n_pai.seq_acoes+"Direita" # a sequencia de ações que gerou o filho é a mesma do pai, acrescentado do deslocamento para a direita
            n_pai.cont_filhos = n_pai.cont_filhos+1 # incrementa quantos filhos o pai gerou
            # insere o filho na primeira posição da borda
            filho_D = No(id_filho_D,custo_filho_D,pos_aspirador_filho_D,estado_A_filho_D,estado_B_filho_D,level_filho_D,acao_filho_D,0)
            fila.insert(0,filho_D)
            # insere o id criado do filho na lista de ids
            lista_borda.append(id_filho_D)
            # insere o pai na segunda posição da borda pois ele vai ser tratado novamente
            pai_D = No(n_pai.id, n_pai.custo, n_pai.pos_aspirador, n_pai.estado_A, n_pai.estado_B, n_pai.level, n_pai.seq_acoes, n_pai.cont_filhos)
            fila.insert(1,pai_D)
            print ("filho D",n_pai.id,id_filho_D,pos_aspirador_filho_D, estado_A_filho_D, estado_B_filho_D, level_filho_D, acao_filho_D)
            return fila, lista_borda

# se o nível do pai for igual ou maior que 4, só retorna a borda sem expandir nenhum filho
    else:
        return fila, lista_borda


def busca_profundidade(borda):
    fila_L = borda
    fila_L = fila_L[1:] # desloca a borda para a segunda posição dela, excluindo a primeira posiçao
    return fila_L

# testa_borda é a função que recebe a borda e testa se ela possui algum elemento armazenado dentro dela
def testa_borda(borda):
    if (len(borda) == 0):
        return False
    else:
        return True

if __name__ == '__main__':
    pos_aspirador= input ("Digite a posicao inicial do aspirador (A=0, B=1):")
    estado_A = input ("Digite como A se encontra (Limpo=0, Sujo=1):")
    estado_B = input ("Digite como B se encontra (Limpo=0, Sujo=1):")

    #criação do nó inicial
    raiz = No(0,0,pos_aspirador,estado_A, estado_B, 0, "0",0)
    borda = [] # criação da borda
    borda.append(raiz) # adiciona o nó raiz na borda
    condicao_borda = True #condição que controla se a borda existe
    #como a raíz já foi adicionada na borda, então condição borda começa em TRUE
    lista_borda = [] # variável que armazena os ids de nós já criados
    lista_borda.append(raiz.id) #adiciona o id da raiz na lista de ids



    while (condicao_borda == True):
        #testa_borda = função que testa se existe alguma coisa na borda
        condicao_borda = testa_borda(borda)
        if (condicao_borda == False):
            break # se nao existe nada na borda, o programa aborta
        #apos ser testada a borda, vamos explorar o primeiro nó da fila
        cur_no = borda[0]
        print (cur_no.id, cur_no.custo, cur_no.pos_aspirador, cur_no.estado_A, cur_no.estado_B, cur_no.level, cur_no.seq_acoes)
        # teste para ver se o nó explorado no momento é o nó objetivo
        if((cur_no.estado_A == 0) and(cur_no.estado_B == 0)): # não importa a posição que o aspirador esteja, desde que os dois espaços estejam limpos
            print ("Numero do no objetivo:",cur_no.id)
            print("Custo do caminho:", cur_no.custo)
            print("Posicao aspirador(A=0,B=1):",cur_no.pos_aspirador)
            print("Estado de A (Limpo=0, Sujo=1):",cur_no.estado_A)
            print("Estado de B (Limpo=0, Sujo=1):",cur_no.estado_B)
            print("Nivel do no objetivo:",cur_no.level)
            print("Sequencia de acoes:",cur_no.seq_acoes)
            break
        else:
        # se o nó explorado no momento nao é o nó objetivo
        # deve-se atualizar a borda retirando esse elemento dela
            borda=busca_profundidade(borda)
        # deve-se expandir esse nó
            borda, lista_borda = gera_filhos_busca_profundidade(cur_no, borda, lista_borda) #função que gera os filhos do nó explorado e armazena na borda
            print(lista_borda)
