import sys


class No (object):

    def __init__(self, id, custo, pos_aspirador, estado_A, estado_B, level, seq_acoes):
        self.id = id
        self.custo = custo
        self.pos_aspirador = pos_aspirador
        self.estado_A = estado_A
        self.estado_B = estado_B
        self.level = level
        self.seq_acoes = seq_acoes


def gera_filhos_busca_largura(n_pai, borda, lista_borda):
    id_pai = n_pai.id
    ultimo_id = max(lista_borda)
##  Estado_pai = [pos_aspirador,estado_A,estado_B]
##  Acoes = ["Esquerda", "Limpar","Direita"]
    fila = borda

    # Esquerda
    id_filho_E = ultimo_id+1
    pos_aspirador_filho_E = 0
    estado_A_filho_E = n_pai.estado_A
    estado_B_filho_E = n_pai.estado_B
    custo_filho_E = n_pai.custo + 1
    level_filho_E = n_pai.level+1
    acao_filho_E = n_pai.seq_acoes +"Esquerda"
    fila.append(No(id_filho_E,custo_filho_E,pos_aspirador_filho_E,estado_A_filho_E,estado_B_filho_E,level_filho_E,acao_filho_E))
    lista_borda.append(id_filho_E)
    #print ("filho E",id_pai,id_filho_E,pos_aspirador_filho_E, estado_A_filho_E, estado_B_filho_E, level_filho_E, acao_filho_E)


    # Limpar
    id_filho_L = ultimo_id+2
    pos_aspirador_filho_L = n_pai.pos_aspirador
    if (pos_aspirador_filho_L == 0):
        estado_A_filho_L = 0
        estado_B_filho_L = n_pai.estado_B
    else:
        estado_A_filho_L = n_pai.estado_A
        estado_B_filho_L = 0
    custo_filho_L = n_pai.custo+1
    level_filho_L = n_pai.level+1
    acao_filho_L = n_pai.seq_acoes + "Limpar"
    fila.append(No(id_filho_L,custo_filho_L,pos_aspirador_filho_L,estado_A_filho_L,estado_B_filho_L,level_filho_L,acao_filho_L))
    lista_borda.append(id_filho_L)
    #print ("filho L",id_pai,id_filho_L,pos_aspirador_filho_L, estado_A_filho_L, estado_B_filho_L, level_filho_L, acao_filho_L)


    # Direita
    id_filho_D = ultimo_id+3
    pos_aspirador_filho_D = 1
    estado_A_filho_D = n_pai.estado_A
    estado_B_filho_D = n_pai.estado_B
    custo_filho_D = n_pai.custo + 1
    level_filho_D = n_pai.level+1
    acao_filho_D = n_pai.seq_acoes+"Direita"
    fila.append(No(id_filho_D,custo_filho_D,pos_aspirador_filho_D,estado_A_filho_D,estado_B_filho_D,level_filho_D,acao_filho_D))
    lista_borda.append(id_filho_D)
    #print ("filho D",id_pai,id_filho_D,pos_aspirador_filho_D, estado_A_filho_D, estado_B_filho_D, level_filho_D, acao_filho_D)

    return fila, lista_borda


def busca_largura(borda):
    fila_L = borda
    fila_L = fila_L[1:]
    return fila_L


# testa_borda é a função que recebe a borda e testa se ela possui algum elemento armazenado dentro dela
def testa_borda(borda):
    if (len(borda) == 0):
        return False
    else:
        return True




if __name__ == '__main__':

    pos_aspirador= input ("Digite a posicao inicial do aspirador (A=0, B=1):")
    estado_A = input ("Digite como A se encontra (Limpo=0, Sujo=1):")
    estado_B = input ("Digite como B se encontra (Limpo=0, Sujo=1):")

    #criação do nó inicial
    raiz = No(0,0,pos_aspirador,estado_A, estado_B, 0, "0")
    borda = [] # criação da borda
    borda.append(raiz) # adiciona o nó raiz na borda
    condicao_borda = True #condição que controla se a borda existe
    lista_borda = []
    lista_borda.append(raiz.id)
    #como a raíz já foi adicionada na borda, então condição borda começa em TRUE

    while (condicao_borda == True):
        #testa_borda = função que testa se existe alguma coisa na borda
        condicao_borda = testa_borda(borda)
        if (condicao_borda == False):
            break # se nao existe nada na borda, o programa aborta
        #apos ser testada a borda, vamos explorar o primeiro nó da fila
        cur_no = borda[0]
        #print (cur_no.id, cur_no.custo, cur_no.pos_aspirador, cur_no.estado_A, cur_no.estado_B, cur_no.level, cur_no.seq_acoes)
        # teste para ver se o nó explorado no momento é o nó objetivo
        if((cur_no.estado_A == 0) and(cur_no.estado_B == 0)): # não importa a posição que o aspirador esteja, desde que os dois espaços estejam limpos
            print ("Numero do no objetivo:",cur_no.id)
            print("Custo do caminho:", cur_no.custo)
            print("Posicao aspirador(A=0,B=1):",cur_no.pos_aspirador)
            print("Estado de A (Limpo=0, Sujo=1):",cur_no.estado_A)
            print("Estado de B (Limpo=0, Sujo=1):",cur_no.estado_B)
            print("Nivel do no objetivo:",cur_no.level)
            print("Sequencia de acoes:",cur_no.seq_acoes)
            break
        else:
        # se o nó explorado no momento nao é o nó objetivo
        # deve-se atualizar a borda retirando esse elemento dela
            borda=busca_largura(borda)
        # deve-se expandir esse nó
            borda, lista_borda = gera_filhos_busca_largura(cur_no, borda, lista_borda) #função que gera oss filhos do nó explorado e armazena na borda



   # print (borda[0].id, borda[0].custo, borda[0].pos_aspirador, borda[0].estado_A, borda[0].estado_B, borda[0].level)
